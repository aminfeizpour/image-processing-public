# -*- coding: utf-8 -*-
"""
Created on Fri Nov  3 11:11:21 2017

@author: af354
"""

#############################################################################
import numpy as np

import javabridge as jv
import bioformats as bf

def read_oib(path):
    '''
    Reads in a .oib file into a numpy array in the order xyzc. Time series are
    not read in as part of this function at this time.
    -------
    path: string
        Complete path the file to open

    '''
    #Open file for reading

    rdr = bf.ImageReader(path)

    # Reading in file parameters
    numC = rdr.rdr.getSizeC()
    numZ = rdr.rdr.getSizeZ()
    sizeX = rdr.rdr.getSizeX()
    sizeY = rdr.rdr.getSizeY()

    # Preallocating the matrix
    readImage = np.empty([sizeX,sizeY,numZ, numC])
    # Reading in
    for c in range(0,numC):
        for z in range(0,numZ):
            readImage[:,:,z,c] = rdr.read(c=c,z=z, rescale=False)

    # Close file
    rdr.close()

    return readImage

# Start the JavaVM for Bioformats
jv.start_vm(class_path=bf.JARS)

##############################################################################
image=read_oib('C:\\Users\\af354\Desktop\\temp\\LEO1_830_2.oib')

a=image[100:400,80:230,0,0]
b=image[100:400,370:450,0,0]

CARS_avg = np.mean(a)
nr_bg = np.mean(b)

CARS_norm = CARS_avg/nr_bg

print("Normalized CARS = ", CARS_norm)
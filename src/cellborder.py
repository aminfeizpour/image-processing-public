# -*- coding: utf-8 -*-
"""
Created on Thu Dec 21 12:09:57 2017

@author: af354
"""

import glob
import os
import numpy as np
import javabridge as jv
import bioformats as bf
import matplotlib

def read_oib(path):
    '''
    From Conor Evans.
    Reads in a .oib file into a numpy array in the order xyzc. Time series are
    not read in as part of this function at this time.
    -------
    path: string
        Complete path the file to open

    '''
    #Open file for reading

    rdr = bf.ImageReader(path)

    # Reading in file parameters
    numC = rdr.rdr.getSizeC()
    numZ = rdr.rdr.getSizeZ()
    sizeX = rdr.rdr.getSizeX()
    sizeY = rdr.rdr.getSizeY()

    # Preallocating the matrix
    readImage = np.empty([sizeX,sizeY,numZ, numC])
    # Reading in
    for c in range(0,numC):
        for z in range(0,numZ):
            readImage[:,:,z,c] = rdr.read(c=c,z=z, rescale=False)

    # Close file
    rdr.close()

    return readImage

# Start the JavaVM for Bioformats
jv.start_vm(class_path=bf.JARS)

testDirectory = 'C:\\Users\\af354\\Dropbox (Partners HealthCare)\\Project Files\\CARS-SRS\20180109_calibration\\calibration\\'
#testPath = 'C:\\Users\\conor\\Documents\\MGH\\Projects\\Active\\CARS-SFA-Melanin\\data\\example\\SKMEL5_Well4_Drug_40kSeed_Area3_1xZoom.oib'

fileDirectory = testDirectory


# Read in list of .oib files
oibFiles = glob.glob(os.path.join(fileDirectory, '*.oib'))



n = -1
stack = np.empty([512,512,len(oibFiles),2])
for file in oibFiles:
    allBlobs = []
    try:
        print("Opening ",file )
        openImage = read_oib(file)
        #the 4th index in openImage shows CARS/SRS
        n = n+1
        stack[:,:,n,:] = openImage[:,:,0,:]
            #if len(openImage) == 0:
                #continue
    except:
        print("Error Opening or Processing File")
        
cars = openImage[:,:,0,0]

matplotlib.pyplot.matshow(cars)


# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 13:17:47 2018

@author: af354
"""

#############################################################################

import glob
import os
import numpy as np
import javabridge as jv
import bioformats as bf
    
def read_oib(path):
    '''
    From Conor Evans.
    Reads in a .oib file into a numpy array in the order xyzc. Time series are
    not read in as part of this function at this time.
    -------
    path: string
        Complete path the file to open

    '''
    #Open file for reading

    rdr = bf.ImageReader(path)

    # Reading in file parameters
    numC = rdr.rdr.getSizeC()
    numZ = rdr.rdr.getSizeZ()
    sizeX = rdr.rdr.getSizeX()
    sizeY = rdr.rdr.getSizeY()

    # Preallocating the matrix
    readImage = np.empty([sizeX,sizeY,numZ, numC])
    # Reading in
    for c in range(0,numC):
        for z in range(0,numZ):
            readImage[:,:,z,c] = rdr.read(c=c,z=z, rescale=False)

    # Close file
    rdr.close()

    return readImage

# Start the JavaVM for Bioformats
jv.start_vm(class_path=bf.JARS)

testDirectory = 'C://Users//af354//Dropbox (Partners HealthCare)//Project Files//CARS-SRS//20180206_LEO-1//f1_845//'
#testPath = 'C:\\Users\\conor\\Documents\\MGH\\Projects\\Active\\CARS-SFA-Melanin\\data\\example\\SKMEL5_Well4_Drug_40kSeed_Area3_1xZoom.oib'

fileDirectory = testDirectory


# Read in list of .oib files
oibFiles = glob.glob(os.path.join(fileDirectory, '*.oib'))



n = -1
stack = np.empty([512,512,len(oibFiles),2])
for file in oibFiles:
    allBlobs = []
    try:
        print("Opening ",file )
        openImage = read_oib(file)
        #the 4th index in openImage shows CARS/SRS
        n = n+1
        stack[:,:,n,:] = openImage[:,:,0,:]
            #if len(openImage) == 0:
                #continue
    except:
        print("Error Opening or Processing File")
        
#define the desired region in the image to be used
     
cars = np.empty([len(oibFiles)])
srs = np.empty([len(oibFiles)])
xaxis = np.empty([len(oibFiles)])

for i in range(0,len(oibFiles)):

    #set the ranges according to the desired areas (signal, background, etc.)
            #stack[y,x,z,c]

    xsig1=50; xsig2=450; ysig1=200; ysig2=400; xbg1=50; xbg2=200; ybg1=375; ybg2=475

    a=stack[ysig1:ysig2,xsig1:xsig2,i,:]
    p = 0
    inten_c = 0
    inten_s = 0
    for j in range(0,a.shape[0]):
        for k in range(0,a.shape[1]):
            if a[j,k,0] > 1000:
               p = p+1
               inten_c = inten_c+a[j,k,0]
               inten_s = inten_s+a[j,k,1]

    if p == 0:
        cars[i] = 0
        srs[i] = 0
    else:
        cars[i] = inten_c/p
        srs[i] = inten_s/p
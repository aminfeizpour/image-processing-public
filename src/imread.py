# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 15:21:11 2017

@author: af354
"""

def imread(filename, *args, **kwargs):
    """Return image data from OIF or OIB file as numpy array.

    'args' and 'kwargs' are arguments to OifFile.asarray().

    Examples
    --------
    >>> image = imread('test.oib')
    >>> image = imread('test/ha#18.oif')

    """
    with OifFile(filename) as oif:
        result = oif.asarray(*args, **kwargs)
    return result
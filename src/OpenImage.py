# -*- coding: utf-8 -*-
"""
Created on Fri Nov  3 10:05:19 2017

@author: af354
"""

import glob
import os

import numpy as np

import javabridge as jv
import bioformats as bf

def read_oib(path):
    '''
    Reads in a .oib file into a numpy array in the order xyzc. Time series are
    not read in as part of this function at this time.
    -------
    path: string
        Complete path the file to open

    '''
    #Open file for reading

    rdr = bf.ImageReader(path)

    # Reading in file parameters
    numC = rdr.rdr.getSizeC()
    numZ = rdr.rdr.getSizeZ()
    sizeX = rdr.rdr.getSizeX()
    sizeY = rdr.rdr.getSizeY()

    # Preallocating the matrix
    readImage = np.empty([sizeX,sizeY,numZ, numC])
    # Reading in
    for c in range(0,numC):
        for z in range(0,numZ):
            readImage[:,:,z,c] = rdr.read(c=c,z=z, rescale=False)

    # Close file
    rdr.close()

    return readImage

# Start the JavaVM for Bioformats
jv.start_vm(class_path=bf.JARS)

testDirectory = 'C:\\'
#testPath = 'C:\\Users\\conor\\Documents\\MGH\\Projects\\Active\\CARS-SFA-Melanin\\data\\example\\SKMEL5_Well4_Drug_40kSeed_Area3_1xZoom.oib'

fileDirectory = testDirectory
'''#print(fileDirectory)

#Prepare output file for writing
outFile = open(os.path.join(fileDirectory,'MelanosomeCounterOutput.csv'), "w")
simpleOutFile = open(os.path.join(fileDirectory,'SimpleMelanosomeCounterOutput.csv'), "w")
#Write in initial values
outFile.write('FileName, X, Y, Radius, channel Ratio, Z\n') '''

# Read in list of .oib files
oibFiles = glob.glob(os.path.join(fileDirectory, '*.oib'))

for file in oibFiles:
#    allBlobs = []
    try:
        print("Opening ",file )
        openImage = read_oib(file)
    except:
        print("Error Opening or Processing File")

#print(openImage) #Displays output to user
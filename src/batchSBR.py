# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 16:43:03 2018

@author: af354
"""

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg
import imageio
import glob
import pylab
import cv2
def kSmallest(arr, k):
    # Sort the given array arr
    arr.sort()
    #Print the first kth largest elements
    kSmall = np.zeros(k)
    for i in range(k):
        kSmall[i] = arr[i] 
    return kSmall
def kLargest(arr, k):
    # Sort the given array arr in reverse order.
    arr.sort()
    arr = np.flip(arr, axis = 0)
    #Print the first kth largest elements
    kLarge = np.zeros(k)
    for i in range(k):
        kLarge[i] = arr[i] 
    return kLarge
def signaltobg(a):
    b = a.ravel() #converting 2d to 1d array
    sbr = np.average(kLargest(b,30000))/np.average(kSmallest(b,30000))
    return sbr
def batchSBR(a):
    p = 0
    snr = np.zeros((np.size(glob.glob(a)), 2))
    for im_path in glob.glob(a):
        im = imageio.imread(im_path)
        c = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
       #print(p)
        snr[p,:] = [p+1, signaltobg(c)]
        p = p+1
    pylab.show(plt.plot(snr[:,0], snr[:,1]))
    pylab.show(plt.hist(snr[:,1], bins=30))
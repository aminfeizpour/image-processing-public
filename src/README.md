# aminfeizpour-public

## batchSBR
This function package is a set of functions for converting PNG images from 16-bit RGB to 16-bit Gray, finding the highest and the lowest pixels in the image, and then the batch function batchSBR(path) calculates a singal to background ratio (SBR) for all the PNG images stored in a directory.

